require('dotenv').config({ path: require('find-config')('.env') })
const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose')
const router = require('./router/index')
const errorMiddleware = require('./middlewares/error-middleware')
var https = require('https');
var fs = require('fs');

const PORT = process.env.PORT
const app = express()

app.use(express.json())
app.use(cookieParser())
app.use(cors({
  credentials: true,
  origin: process.env.CLIENT_URL
}));
app.use('/api', router)
app.use(errorMiddleware)

const httpsOptions = {
  key: fs.readFileSync('./security/cert.key'),
  cert: fs.readFileSync('./security/cert.pem')
}


const server = https.createServer(httpsOptions, app).listen(PORT, () => {
  console.log('server running at ' + PORT)
})

const start = async () => {
  try {
    // server()
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    app.listen(PORT, () => console.log('started on ', PORT))
  } catch (e) {
    console.log(e)
  }
}

start()